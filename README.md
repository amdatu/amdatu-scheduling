# Amdatu Scheduling

Amdatu Scheduling makes it easy to schedule jobs that are executed at configured intervals or dates. The component is based on the Quartz framework, and adds a whiteboard style approach to registering jobs.

## Usage
Please visit the [Scheduling](http://www.amdatu.org/components/scheduling.html) component page on the Amdatu website for a detailed description on how to use Amdatu Scheduling.

## Building

The source code can be built on the command line using Gradle, or by using [Bndtools](http://bndtools.org/) in Eclipse.
Tests can be ran from command line using Gradle as well, or by using JUnit in Eclipse.

### Eclipse using Bndtools
When Bndtools is correctly installed in Eclipse, import all subprojects into the workspace using `Import -> Existing Projects into Workspace` from the Eclipse context menu. Ensure `project -> Build Automatically` is checked in the top menu, and when no (compilation) errors are present, each subproject's bundles should be available in their `generated` folder.

###Gradle
When building from command line, invoke `./gradlew jar` from the root of the project. This will assemble all project bundles and place them in the `generated` folder of each subproject.


## Testing

### Eclipse using Bndtools
Unit tests can be ran per project or per class, using JUnit in Eclipse. Unit tests are ran by right clicking a project which contains a non-empty test folder or a test class and choosing `Run As -> JUnit Test`.

Integration tests can be ran in a similar way; right click an integration test project (commonly suffixed by .itest), but select `Run As -> Bnd OSGi Test Launcher (JUnit)` instead.

### Gradle
Unit tests are ran by invoking `./gradlew test` from the root of the project.

Integration tests can be ran by running a full build, this is done by invoking `./gradlew build` from the root of the project.
More info on available Gradle tasks can be found by invoking `./gradlew tasks`.
	
## Links

* [Source Code](https://bitbucket.org/amdatu/amdatu-scheduling);
* [Issue Tracking](https://amdatu.atlassian.net/browse/AMDATUSCH);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Scheduling)
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/AMDATUSCH).

## License

The Amdatu Scheduling project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

